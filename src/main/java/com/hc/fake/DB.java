package com.hc.fake;

import com.google.common.collect.Sets;
import com.hc.domain.Permission;
import com.hc.domain.Role;
import com.hc.domain.User;
import com.hc.util.HcUtil;
import org.apache.commons.lang3.RandomStringUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DB {
    public static List<User> users = new ArrayList<>();
    public static Map<Integer, Set<Role>> roles = new HashMap<>();
    public static Map<Integer, Set<Permission>> permissions = new HashMap<>();

    //盐值和加密后的密码可以从ShiroUtil类的main方法中获取
    static {
        User user1 = new User(1001, "zhangsan", "85f85c2fac14729552444052d12c2812", "张先生", "XxnbFhqI", false, LocalDateTime.now(), LocalDateTime.now());
        User user2 = new User(1002, "lisi", "bf06cf274733c4fd15bdbb0ffbd40d7a", "李女士", "rVLCYWxi", false, LocalDateTime.now(), LocalDateTime.now());
        User user3 = new User(1003, "wanger", "1f2ec4c40e3c61532de787db69534974", "王女士", "dcDzuoWt", false, LocalDateTime.now(), LocalDateTime.now());

        users.add(user1);
        users.add(user2);
        users.add(user3);

        //key是用户编号，值是角色
        roles.put(1001, Sets.newHashSet(new Role(1, "admin"))); //管理员
        roles.put(1002, Sets.newHashSet(new Role(2, "user"))); //普通用户
        roles.put(1003, Sets.newHashSet(new Role(3, "naive"))); //初级用户

        //key是角色编号，值是权限
        permissions.put(1, Sets.newHashSet(new Permission(11, "user:*:*"))); //格式  资源标识符：操作权限：资源类型/资源实例，比如user:*:*指对user下面的所有资源有所有的操作权限
        permissions.put(2, Sets.newHashSet(new Permission(22, "goods:update:*"), new Permission(33, "goods:delete:*")));
        permissions.put(3, Sets.newHashSet());
    }

    public static void main(String[] args) {
        //为用户生成随机的盐值
        System.out.println(RandomStringUtils.randomAlphabetic(8));
        System.out.println(RandomStringUtils.randomAlphabetic(8));

        //生成加盐后的密码
        String pwd = HcUtil.saltPassword("123456", "XxnbFhqI");
        System.out.println(pwd);

        String pwd2 = HcUtil.saltPassword("123456", "rVLCYWxi");
        System.out.println(pwd2);

        String pwd3 = HcUtil.saltPassword("123456", "dcDzuoWt");
        System.out.println(pwd3);
    }

}
