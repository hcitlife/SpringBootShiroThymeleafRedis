package com.hc.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
public class Role implements Serializable {
    private static final long serialVersionUID = -583226926296215616L;
    /**
     * 编号
     */
    private Integer id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色关键字
     */
    private String code;

    /**
     * 排序值（默认是50）
     */
    private Byte level;

    /**
     * 备注信息
     */
    private String info;

    /**
     * 启用状态：0禁用  1启用
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    /**
     * 用户所有权限值，用于shiro做资源权限的判断
     * 建议：在角色实体类中定义
     */
    private Set<String> permissions = new HashSet<>();

    public Role(Integer id, String code) {
        this.id = id;
        this.code = code;
    }
}

