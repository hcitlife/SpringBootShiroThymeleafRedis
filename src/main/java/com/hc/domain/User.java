package com.hc.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Data
@AllArgsConstructor
@RequiredArgsConstructor()
public class User implements Serializable {
    private static final long serialVersionUID = -1091190204717346101L;

    /**
     * 主键
     */
    private Integer id;
    /**
     * 登录用户名
     */
    private String username;
    /**
     * 已加密的登录密码
     */
    private String password;
    /**
     * 昵称
     */
    private String nickname;

    /**
     * 加密盐值（必须得保存在数据库中）
     */
    private String salt;

    /**
     * 是否被锁定
     */
    private Boolean locked;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
    /**
     * 修改时间
     */
    private LocalDateTime updateTime;


    public User(Integer id, String name, String password, String nickname, String salt, Boolean locked, LocalDateTime createTime, LocalDateTime updateTime) {
        this.id = id;
        this.username = name;
        this.password = password;
        this.nickname = nickname;
        this.salt = salt;
        this.locked = locked;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * 用户所有角色值，用于shiro做角色权限的判断
     */
    private Set<Role> roles = new HashSet<>();

}