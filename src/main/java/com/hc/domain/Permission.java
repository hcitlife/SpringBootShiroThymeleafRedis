package com.hc.domain;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Data
public class Permission implements Serializable {
    private static final long serialVersionUID = 2706967907870682047L;
    /**
     * 编号
     */
    private Integer id;

    /**
     * 权限标题
     */
    private String name;

    /**
     * 权限关键字
     */
    private String code;

    /**
     * 权限所能访问的资源的路径
     */
    private String url;

    /**
     * 权限所对应的图标
     */
    private String icon;

    /**
     * 排序值（默认是50）
     */
    private Byte level;

    /**
     * 是否展开：0关闭  1展开
     */
    private Boolean isOpen;

    /**
     * 备注信息
     */
    private String info;

    /**
     * 父权限ID，根节点的父权限为空
     */
    private Integer pid;

    /**
     * 启用状态：0禁用  1启用
     */
    private Boolean status;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    private LocalDateTime updateTime;

    public Permission(Integer id, String code) {
        this.id = id;
        this.code = code;
    }
}

