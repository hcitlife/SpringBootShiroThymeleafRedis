package com.hc.cache;

import com.hc.util.ApplicationContextUtil;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.util.Collection;
import java.util.Set;

/**
 * 自定义Redis缓存的实现
 * 注：此处的redisTemplate不能使用@Resouce注解自动注入
 */
public class ShiroRedisCache<K, V> implements Cache<K, V> {
    private String cacheName;

    public ShiroRedisCache() {
    }

    public ShiroRedisCache(String cacheName) {
        this.cacheName = cacheName;
    }

    /**
     * 从Redis中获取缓存内容
     *
     * @param k
     * @return
     * @throws CacheException
     */
    @Override
    public V get(K k) throws CacheException {
        if (k == null) {
            return null;
        }
        System.out.println("get: " + k);
        RedisTemplate redisTemplate = getRedisTemplate();
        //V res = (V) redisTemplate.opsForValue().get(k.toString());
        V res = (V) redisTemplate.opsForHash().get(this.cacheName, k.toString());
        return res;
    }

    private RedisTemplate getRedisTemplate() {
        RedisTemplate redisTemplate = (RedisTemplate) ApplicationContextUtil.getBean("redisTemplate");
        //Map<String,Map<String,Object>>
        redisTemplate.setKeySerializer(new StringRedisSerializer()); //设置外部key序列化方式
        redisTemplate.setHashKeySerializer(new StringRedisSerializer()); //设置内部key序列化方式
        return redisTemplate;
    }

    /**
     * 将数据库中查询到的内容放入Redis缓存
     *
     * @param k
     * @param v
     * @return
     * @throws CacheException
     */
    @Override
    public V put(K k, V v) throws CacheException {
        if (k== null || v == null) {
            return null;
        }
        System.out.println("put: " + k + " : " + v);
        RedisTemplate redisTemplate = getRedisTemplate();
        //redisTemplate.opsForValue().set(k.toString(),v);
        redisTemplate.opsForHash().put(this.cacheName, k.toString(), v);
        return null;
    }

    /**
     * 清空指定的缓存
     * 当用户登录登录时，会自动调用这个方法，为了将登录过的用户的信息缓存起来，所以这个方法直接返回null
     * @param k
     * @return
     * @throws CacheException
     */
    @Override
    public V remove(K k) throws CacheException {
        System.out.println("remove");
        //if(k==null){
        //    return null;
        //}
        //RedisTemplate redisTemplate = getRedisTemplate();
        //V res  = (V) redisTemplate.opsForHash().delete(this.cacheName,k.toString());
        //return res;
        return null;
    }

    @Override
    public void clear() throws CacheException {
        System.out.println("clear");
        RedisTemplate redisTemplate = getRedisTemplate();
        redisTemplate.opsForHash().delete(this.cacheName);//删除整个Map
    }

    @Override
    public int size() {
        RedisTemplate redisTemplate = getRedisTemplate();
        Long res = redisTemplate.opsForHash().size(this.cacheName);
        return res.intValue();
    }

    @Override
    public Set<K> keys() {
        RedisTemplate redisTemplate = getRedisTemplate();
        Set<K> res = redisTemplate.opsForHash().keys(this.cacheName);
        return res;
    }

    @Override
    public Collection<V> values() {
        RedisTemplate redisTemplate = getRedisTemplate();
        Collection<V> res = redisTemplate.opsForHash().values(this.cacheName);
        return res;
    }
}
