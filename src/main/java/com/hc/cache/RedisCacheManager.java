package com.hc.cache;

import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.apache.shiro.cache.CacheManager;

/**
 * 自定义Shiro缓存管理器
 */
public class RedisCacheManager implements CacheManager {
    /**
     * @param cacheName 在ShiroConfig的getUserRealm()方法中指定的认证/授权缓存的统一名称
     * @param <K>
     * @param <V>
     * @return
     * @throws CacheException
     */
    @Override
    public <K, V> Cache<K, V> getCache(String cacheName) throws CacheException {
        System.out.println("缓存名称： "+cacheName);
        return new ShiroRedisCache(cacheName);
    }

}
