package com.hc.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 作用：打开网页
 */
@Controller
public class DispatchController {
    @RequestMapping("/index")
    public String index(Model model){
        model.addAttribute("msg","Hello,Shiro");
        return "index";
    }

    @RequestMapping("/regist")
    public String regist(){
        return "regist";
    }

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/res")
    public String res(){
        return "res";
    }

    @RequestMapping("/unauth")
    public String unauth(){
        return "unauth";
    }

}
