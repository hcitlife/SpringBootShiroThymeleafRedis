package com.hc.controller;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping("/goods")
public class GoodsController {

    /**
     * 代码授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/save")
    public String save(Model model) {
        Subject subject = SecurityUtils.getSubject();
        //基于角色
        if (subject.hasRole("user")) {
            model.addAttribute("msg", "成功增加商品");
        } else {
            model.addAttribute("msg", "无权增加商品");
        }
        return "res";
    }

    /**
     * 代码授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/update")
    public String update(Model model) {
        Subject subject = SecurityUtils.getSubject();
        //基于资源
        if (subject.isPermitted("goods:update:*")) {
            model.addAttribute("msg", "成功更新商品");
        } else {
            model.addAttribute("msg", "无权更新商品");
        }
        return "res";
    }

    /**
     * 代码授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/find")
    public String find(Model model) {
        Subject subject = SecurityUtils.getSubject();
        //基于资源
        if (subject.isPermitted("goods:find:*")) {
            model.addAttribute("msg", "成功查看商品");
        } else {
            model.addAttribute("msg", "无权查看商品");
        }
        return "res";
    }

    /**
     * 注解授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/delete1")
    @RequiresRoles("admin") //基于角色
    public String delete1(Model model) {
        model.addAttribute("msg", "成功删除商品1");
        return "res";
    }
    /**
     * 注解授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/delete2")
    @RequiresRoles(value = {"admin","user"},logical = Logical.OR) //基于角色
    public String delete2(Model model) {
        model.addAttribute("msg", "成功删除商品2");
        return "res";
    }
    /**
     * 注解授权
     * <p>
     * 测试步骤：先登录，成功之后，在地址栏输入这个路径再请求
     *
     * @param model
     * @return
     */
    @RequestMapping("/delete3")
    @RequiresPermissions("goods:delete:*") //基于资源
    public String delete3(Model model) {
        model.addAttribute("msg", "成功删除商品3");
        return "res";
    }
}
