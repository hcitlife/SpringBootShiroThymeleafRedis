package com.hc.service;

import com.hc.domain.Permission;
import com.hc.domain.User;
import com.hc.fake.DB;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class PermissionService {
    /**
     * 查询指定角色所拥有的权限
     *
     * @param roleId
     * @return
     */
    public Set<Permission> getPermissionsByRoleId(Integer roleId) {
        System.out.println("模拟操作数据库： tb_permission");
        Set<Permission> permissions = DB.permissions.get(roleId);
        return permissions;
    }
}
