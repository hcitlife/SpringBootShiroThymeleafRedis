package com.hc.service;

import com.hc.fake.DB;
import com.hc.domain.User;
import com.hc.util.HcUtil;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Date;

@Service
public class UserService {

    public void regist(User user){
        String salt = HcUtil.genSalt(8);
        //将随机盐保存到数据库
        user.setSalt(salt);
        //重点：对明文密码进行MD5+salt+hash散列
        Md5Hash md5Hash = new Md5Hash(user.getPassword(), salt, HcUtil.SHIRO_DECODE_ITERATIONS);
        //设置密码为加密后的值
        user.setPassword(md5Hash.toHex());
        user.setLocked(false);
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());

        //模拟将用户信息保存到数据库
        System.out.println(user);
    }

    public User getUserByUsername(String username) {
        System.out.println("模拟操作数据库： tb_user");
        for (User user : DB.users) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }

}
