package com.hc.service;

import com.hc.domain.Role;
import com.hc.domain.User;
import com.hc.fake.DB;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class RoleService {
    /**
     * 查询指定id的用户所拥有的角色
     * @param userId
     * @return
     */
    public Set<Role> getRolesByUserId(Integer userId) {
        System.out.println("模拟操作数据库： tb_role");
        Set<Role> roles = DB.roles.get(userId);
        return roles;
    }
}
