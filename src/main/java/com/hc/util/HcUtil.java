package com.hc.util;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class HcUtil {

    public static int SHIRO_DECODE_ITERATIONS = 1024;

    /**
     * 生成指定位数的随机盐
     */
    public static String genSalt(Integer length){
        return  RandomStringUtils.randomAlphabetic(length);
    }

    /**
     * 加盐加密
     */
    public static String saltPassword(String crdentials, String salt) {
        ByteSource byteSource = ByteSource.Util.bytes(salt);
        SimpleHash simpleHash = new SimpleHash("MD5", crdentials, byteSource, SHIRO_DECODE_ITERATIONS);
        return simpleHash.toString();
    }

}
