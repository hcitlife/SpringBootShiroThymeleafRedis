package com.hc.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class ApplicationContextUtil implements ApplicationContextAware {
    private static ApplicationContext act;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        act = applicationContext;
    }

    /**
     * 根据bean的名字获取工厂中对应的bean对象
     *
     * @param beanName
     * @return
     */
    public static Object getBean(String beanName) {
        return act.getBean(beanName);
    }

}