package com.hc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootShiroThymeleafApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootShiroThymeleafApplication.class, args);
    }

}
